titre: Les manipulations de chaines de caractères

01 La longueur d'une chaine de caractères
02 Concatener deux chaines de caractères
03 Supprimer des caractères dans une chaine de caractères
04 Remplacer des caractères dans une chaine de caractères

Éléments de contenu: 
1. Apprendre à manipuler les chaines de caractères.
