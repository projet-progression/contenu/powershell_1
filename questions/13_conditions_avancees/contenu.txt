titre: Les notions avancées sur les conditions

01 - L'instruction switch (compléter le script)
02 - L'instruction switch (construire le script)
03 - Un IF complexe avec un AND
04 - Un IF complexe avec un OR
05 - Un IF complexe avec un AND et un OR

Éléments de contenu: 
1. Apprendre à utiliser l'instruction case pour brancher au lieu d'utiliser de multiples IF
2. Apprendre à utiliser des formes complexes de conditions IF
