titre: Les variables et les entrées

01 - une assignation simple
02 - afficher le contenu d'une variables
03 - lire une information sur l'entrée standard (standard input, par défaut le clavier)
04 - capturer la date du jour dans une variable

Éléments de contenu: 
1. Apprendre à utiliser des variables.
2. Apprendre à lire de l'information de l'entrée standard (standard input).
3. Apprendre à capturer la sortie d'une commande dans une variable.
